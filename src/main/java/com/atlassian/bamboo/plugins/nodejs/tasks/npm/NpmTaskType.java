package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.ExecutableBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkState;

public class NpmTaskType implements CommonTaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String NPM_ISOLATED_CACHE_DIRECTORY = "npm.isolated.cache.directory";

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final I18nResolver i18nResolver;
    private final AgentContext agentContext;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public NpmTaskType(final ProcessService processService,
                       final EnvironmentVariableAccessor environmentVariableAccessor,
                       final CapabilityContext capabilityContext,
                       final I18nResolver i18nResolver,
                       final AgentContext agentContext)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.i18nResolver = i18nResolver;
        this.agentContext = agentContext;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull CommonTaskContext taskContext) throws TaskException
    {
        try
        {
            final BuildLogger buildLogger = taskContext.getBuildLogger();
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

            final String nodeRuntime = configurationMap.get(NpmConfigurator.NODE_RUNTIME);
            final String nodePath = capabilityContext.getCapabilityValue(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "." + nodeRuntime);
            checkState(StringUtils.isNotBlank(nodePath), i18nResolver.getText("node.runtime.error.undefinedPath"));

            final String npmPath = convertNodePathToNpmPath(nodePath);
            final String command = configurationMap.get(NpmConfigurator.COMMAND);
            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final List<String> arguments = CommandlineStringUtils.tokeniseCommandline(command);
            final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder().add(npmPath);
            commandListBuilder.addAll(arguments);

            final boolean useIsolatedCache = configurationMap.getAsBoolean(NpmConfigurator.ISOLATED_CACHE);
            if (useIsolatedCache)
            {
                final String cacheDirectory = getTemporaryCacheDirectory(taskContext, agentContext);
                buildLogger.addBuildLogEntry(i18nResolver.getText("npm.isolatedCache.usage", cacheDirectory));
                commandListBuilder.add("--cache", cacheDirectory);
            }

            final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
            taskResultBuilder.checkReturnCode(processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandListBuilder.build())
                    .env(extraEnvironmentVariables)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * A helper method to transform Node.js executable path to npm path.
     * <p/>
     * Note: this solution is ugly, but it works (since usually Node and npm executables are stored in the same
     * directory). Best way would be to create a separate, independent capability for npm, but that would add
     * unnecessary complexity.
     *
     * @param nodePath path to Node.js executable
     *
     * @return a transformed path to npm executable
     */
    @NotNull
    protected static String convertNodePathToNpmPath(@NotNull final String nodePath)
    {
        return FilenameUtils.getFullPath(nodePath) + NodeCapabilityDefaultsHelper.NPM_EXECUTABLE_NAME;
    }

    /**
     * Gets an absolute path to a temporary folder for npm isolated cache.
     * <p/>
     * This directory is shared between all npm tasks that are executed within the same {@link CommonContext} (so, for
     * instance, between tasks belonging to one {@link Job}).
     * <p/>
     * During the first call to this method the directory will be created (or at least cleaned). Next calls within one
     * context will fetch the same directory path.
     *
     * @param taskContext  task context
     * @param agentContext agent context
     *
     * @return An absolute path to the npm cache directory.
     *
     * @throws IOException if method fails to create a temporary directory
     */
    @NotNull
    protected static String getTemporaryCacheDirectory(@NotNull CommonTaskContext taskContext, @NotNull AgentContext agentContext) throws IOException
    {
        final Map<String, String> customBuildData = taskContext.getCommonContext().getCurrentResult().getCustomBuildData();
        if (!customBuildData.containsKey(NPM_ISOLATED_CACHE_DIRECTORY))
        {
            final ExecutableBuildAgent buildAgent = agentContext.getBuildAgent();
            checkState(buildAgent != null, "No build agent detected that is currently executing the task");

            final File tempCacheDirectory = NpmIsolatedCacheHelper.initializeTemporaryCacheDirectory(buildAgent.getId());
            customBuildData.put(NPM_ISOLATED_CACHE_DIRECTORY, tempCacheDirectory.getAbsolutePath());
        }
        return customBuildData.get(NPM_ISOLATED_CACHE_DIRECTORY);
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}

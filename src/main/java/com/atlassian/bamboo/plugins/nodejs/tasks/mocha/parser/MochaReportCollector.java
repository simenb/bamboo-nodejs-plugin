package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MochaReportCollector implements TestReportCollector
{
    private static final Logger log = Logger.getLogger(MochaReportCollector.class);

    @Override
    @NotNull
    public TestCollectionResult collect(@NotNull File file) throws Exception
    {
        final Collection<TestResults> pass = Lists.newArrayList();
        final Collection<TestResults> fail = Lists.newArrayList();
        final Collection<TestResults> skip = Lists.newArrayList();

        final MochaSuite results;

        final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        final Reader fileReader = new BufferedReader(new FileReader(file));
        try
        {
            results = gson.fromJson(fileReader, MochaSuite.class);
        }
        finally
        {
            IOUtils.closeQuietly(fileReader);
        }

        for (final MochaSuiteTest test : results.getPasses())
        {
            pass.add(toTestResult(test, TestState.SUCCESS));
        }
        for (final MochaSuiteTest test : results.getFailures())
        {
            fail.add(toTestResult(test, TestState.FAILED));
        }
        for (final MochaSuiteTest test : results.getSkipped())
        {
            skip.add(toTestResult(test, TestState.SKIPPED));
        }

        final TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
        log.info(results.toString());
        builder.addSuccessfulTestResults(pass);
        builder.addFailedTestResults(fail);
        builder.addSkippedTestResults(skip);
        return builder.build();
    }

    @Override
    @NotNull
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("json");
    }

    private static TestResults toTestResult(@NotNull MochaSuiteTest test, @NotNull TestState state)
    {
        final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(test.getDuration());
        final TestResults testResults = new TestResults(test.getFullTitle(), test.getTitle(), String.valueOf(durationInSeconds));
        testResults.setState(state);
        if (TestState.FAILED.equals(state))
        {
            testResults.addError(new TestCaseResultErrorImpl(test.getError()));
        }
        return testResults;
    }

}

package com.atlassian.bamboo.plugins.nodejs.tasks.bower;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.git.GitCapabilityTypeModule;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;
import java.util.Set;

public class BowerConfigurator extends AbstractNodeRequiringTaskConfigurator
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(BowerConfigurator.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String BOWER_DEFAULT_EXECUTABLE = Joiner.on(File.separator).join("node_modules", "bower", "bin", "bower");

    public static final String BOWER_RUNTIME = "bowerRuntime";
    public static final String COMMAND = "command";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(BOWER_RUNTIME)
            .add(COMMAND)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeRequiringTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(BOWER_RUNTIME, BOWER_DEFAULT_EXECUTABLE)
            .put(COMMAND, "install")
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(BOWER_RUNTIME)))
        {
            errorCollection.addError(BOWER_RUNTIME, i18nResolver.getText("bower.runtime.error.empty"));
        }
        if (StringUtils.isBlank(params.getString(COMMAND)))
        {
            errorCollection.addError(COMMAND, i18nResolver.getText("bower.command.error.empty"));
        }
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition)
    {
        return ImmutableSet.<Requirement>builder()
                .addAll(super.calculateRequirements(taskDefinition))
                .add(new RequirementImpl(GitCapabilityTypeModule.GIT_CAPABILITY, true, ".*", true))
                .build();
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @NotNull
    @Override
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}
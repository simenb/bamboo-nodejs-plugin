package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public abstract class ComponentWithAdvancedOptions
{
    @ElementBy(cssSelector = "div.summary h3")
    protected PageElement advancedOptionsHeader;

    @ElementBy(cssSelector = "div.collapsible-details")
    protected PageElement advancedOptions;

    protected void withAdvancedOptions()
    {
        if (!advancedOptions.isVisible())
        {
            advancedOptionsHeader.click();
        }
        Poller.waitUntilTrue(advancedOptions.timed().isVisible());
    }
}